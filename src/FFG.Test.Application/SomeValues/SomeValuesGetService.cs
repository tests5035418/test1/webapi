﻿using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FFG.Test.Application.SomeValues
{
    internal class SomeValuesGetService : ISomeValuesGetService
    {
        private readonly ILogger<SomeValuesGetService> _logger;

        public SomeValuesGetService(ILogger<SomeValuesGetService> logger) 
        {
            _logger = logger;
        }

        public async Task<SomeValuesDto> GetValues()
        {
            return new();
        }
    }
}
