﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FFG.Test.Application.SomeValues
{
    public record struct SomeValuesDto
    {
        public SomeValuesDto() : this(0, 0, 0, null)
        { }

        public SomeValuesDto(int total, int page, int size, ICollection<SomeValueItemDto> data)
        {
            TotalItems = total;
            Page = page;
            Size = size;
            Items = data;
        }

        public int Page { get; init; }

        public int Size { get; init; }

        public int TotalItems { get; init; }

        public bool IsLast => TotalItems <= (Page * Size);

        public ICollection<SomeValueItemDto> Items { get; init; }
    }
}
