﻿using FFG.Test.Application.SomeValues;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Linq;
using System.Threading.Tasks;

namespace FFG.Test.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ValuesController : ControllerBase
    {
        private ILogger<ValuesController> _logger;

        public ValuesController(ILogger<ValuesController> logger) 
        {
            _logger = logger;
        }

        [HttpGet]
        public async Task<ActionResult<SomeValuesDto>> GetValues()
        {
            return Ok(Enumerable.Range(0, 100));
        }

        [HttpPost]
        public async Task<ActionResult> CreateValues()
        {

            return NoContent();
        }
    }
}
